angular.module('flocktApp').factory('userProfileFactory', function($http, $rootScope) {
  return {
    createSession: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/auth/validate_value',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    getCommunitiesList: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/get_data/list',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    updateUserDetails: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/user/update/details',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    setProfileDetails: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/users/create',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    getCommuntiyDetails: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/get_data/aboutCommunity',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    getCommuntiyPosts: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/getCommunityPost',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    getChannelsList: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/channel/getAll',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },
    getMemberDetails: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/users/details',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    getChannelPosts: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/getChannelPost',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    getComments: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/comment/commentdetails',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    addComments: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/comment/write',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    emoteComment: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/emote',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    getCommentReply: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/comment/getCommentReply',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    replyForComment: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/comment/write',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    emotePost: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/emote',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    extractUrl: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/urlExtract',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    createPost: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/create',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    imageUpload: function(params, community) {
      return $http({
        url: $rootScope.baseUrl + '/media/'+community,
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    starUnstarChannel: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/channel/star',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    muteUnmuteChannel: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/channel/mute',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    getMemberStarredChannel: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/channel/starredchannels',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    getPostDetails: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/post/getPostDetails',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    validateUserKey: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/validate_key',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    },

    logout: function(params) {
      return $http({
        url: $rootScope.baseUrl + '/logout',
        method: 'POST',
        data: params,
        headers: {
          'Content-Type': 'application/json'
        }
      })
    }

  }
});
