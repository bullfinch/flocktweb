'use strict';

/**
 * @ngdoc overview
 * @name flocktApp
 * @description
 * # flocktApp
 *
 * Main module of the application.
 */
angular.module('flocktApp', [
  'ngAnimate',
  'ngAria',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'ui.router',
  'ngMaterial',
  'com.2fdevs.videogular',
  'com.2fdevs.videogular.plugins.controls',
  'com.2fdevs.videogular.plugins.overlayplay',
  'angular-svg-round-progressbar',
  'ngDialog',
  'ngMaterialDatePicker',
  'ng.deviceDetector',
  'ngStorage',
  'ui.bootstrap',
  'angularMoment',
  'ngFileUpload',
  'mdPickers'
]);

angular.module('flocktApp').config(function($locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
});
