angular.module('flocktApp').directive("mobileMenu", function() {
  return {
    restrict: "E",
    scope: {},
    templateUrl: "../views/sideBar.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $timeout, $localStorage, $stateParams, userProfileFactory) {

      $scope.$storage = $localStorage;
      $scope.currentCommunity = $stateParams.community_name;

      $scope.init = function() {
        $scope.currentCommunity = $stateParams.community_name;
        $scope.windowSize = $window.innerWidth;
        $scope.windowHeight = $window.innerHeight;
        if ($scope.$storage.userDetails && $scope.$storage.userDetails.user_id) {
          $scope.userLoggedIn = true;
        } else {
          $scope.userLoggedIn = false;
        }
        $scope.starUnstarPayload = {};
        $scope.muteUnmuteChannelPayload = {};
      };

      $scope.closePopup = function() {
        ngDialog.close();
      };

      $scope.clickToOpen = function(id) {
        ngDialog.close();
        ngDialog.open({
          template: id,
          className: 'ngdialog-theme-default sideBarMenu',
          showClose: false,
          scope: $scope
        });
        $scope.currentTab = id;
      }
      $scope.user = null;
      $scope.users = null;

      $scope.channelClick = function(id) {
        ngDialog.close()
        ngDialog.open({
          template: id,
          className: 'ngdialog-theme-default sideBarMenu',
          scope: $scope,
        });
        $scope.currentTab = id;
      };

      $scope.clickToclose = function(id) {
        ngDialog.close()
        $scope.currentTab = id;
      };

      $scope.gotoChannel = function(channelname, channelid) {
        ngDialog.close();
        $location.path('/channel/' + $stateParams.community_name + '/' + channelname + '/' + channelid);
      }

      $scope.$on('loggedOut', function(event) {
        $scope.init();
      });

      $scope.starUnstarChannel = function(channelId, status, index) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.starUnstarPayload.community_name = $stateParams.community_name;
          $scope.starUnstarPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.starUnstarPayload.channel_id = channelId;
          $scope.starUnstarPayload.status = status;
          return userProfileFactory.starUnstarChannel($scope.starUnstarPayload).success(function(response) {
            var starred = {
              "id": $rootScope.channels[index].channelId,
              "cName": $rootScope.channels[index].channel_name,
              "name": $rootScope.channels[index].display_name,
              "star": 1,
              "mute": 0
            };
            $rootScope.starredChannel.push(starred);
            $rootScope.channels.splice(index, 1);
          });
        } else {
          ngDialog.close();
          $rootScope.clickButton('signin');
        }
      };

      $scope.muteUnmuteChannel = function(channelId, status, index) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.muteUnmuteChannelPayload.community_name = $stateParams.community_name;
          $scope.muteUnmuteChannelPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.muteUnmuteChannelPayload.channel_id = channelId;
          $scope.muteUnmuteChannelPayload.status = status;
          return userProfileFactory.muteUnmuteChannel($scope.muteUnmuteChannelPayload).success(function(response) {
            $rootScope.starredChannel[index].mute = status;
          });
        } else {
          ngDialog.close();
          $rootScope.clickButton('signin');
        }

      };

      $scope.init();

    }
  }
});

angular.module('flocktApp').directive("webSidebar", function() {
  return {
    restrict: "EA",
    scope: {},
    templateUrl: "../views/webSidemenu.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $stateParams, userProfileFactory, $localStorage) {

      $scope.$storage = $localStorage;

      $scope.init = function() {
        $rootScope.userName = $scope.$storage.userDetails.name;
        $scope.windowSize = $window.innerWidth;
        $scope.windowHeight = $window.innerHeight - 60;
        $scope.enterNumberSection = true;
        $scope.starUnstarPayload = {};
        $scope.muteUnmuteChannelPayload = {};
        $scope.getStarredChannelPayload = {};
        $rootScope.currentCommunity = $stateParams.community_name;
        $scope.channelPayload = {};
        if ($scope.$storage.userDetails && $scope.$storage.userDetails.hasUser) {
          $scope.userLoggedIn = true;
        } else {
          $scope.userLoggedIn = false;
        }
        $scope.getStarredChannel();
      };

      $scope.gotoChannel = function(channelname, channelid) {
        $location.path('/channel/' + $stateParams.community_name + '/' + channelname + '/' + channelid);
      }

      $scope.$on('updateChannelList', function(event) {
        $scope.init();
      });

      $rootScope.clickButton = function(signame) {
        ngDialog.close()
        ngDialog.open({
          template: signame,
          className: 'ngdialog-theme-default sideBarMenu',
          scope: $scope,
          controller: 'signInController'
        });

        $rootScope.currentTab = signame;
      };

      $scope.getChannels = function() {
        $scope.channelPayload.community_name = $stateParams.community_name;
        $scope.channelPayload.key = $scope.$storage.userDetails.sessionKey;
        return userProfileFactory.getChannelsList($scope.channelPayload).success(function(response) {
          $rootScope.channels = response;
          angular.forEach($rootScope.starredChannel, function(starred, starredIndex) {
            angular.forEach($rootScope.channels, function(channel, channelIndex) {
              if (channel.channel_id == starred.id) {
                $rootScope.channels.splice(channelIndex, 1);
              }
            });
          });
        });
      };

      $scope.getStarredChannel = function() {
        $scope.getStarredChannelPayload.community_name = $stateParams.community_name;
        $scope.getStarredChannelPayload.key = $scope.$storage.userDetails.sessionKey;
        return userProfileFactory.getMemberStarredChannel($scope.getStarredChannelPayload).success(function(response) {
          $rootScope.starredChannel = response.channels;
          $scope.getChannels();
        });
      };

      $scope.starUnstarChannel = function(channelId, status, index) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.starUnstarPayload.community_name = $stateParams.community_name;
          $scope.starUnstarPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.starUnstarPayload.channel_id = channelId;
          $scope.starUnstarPayload.status = status;
          return userProfileFactory.starUnstarChannel($scope.starUnstarPayload).success(function(response) {
            var starred = {
              "id": $rootScope.channels[index].channelId,
              "cName": $rootScope.channels[index].channel_name,
              "name": $rootScope.channels[index].display_name,
              "star": 1,
              "mute": 0
            };
            $rootScope.starredChannel.push(starred);
            $rootScope.channels.splice(index, 1);
          });
        } else {
          ngDialog.close();
          $rootScope.clickButton('signin');
        }
      };

      $scope.muteUnmuteChannel = function(channelId, status, index) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.muteUnmuteChannelPayload.community_name = $stateParams.community_name;
          $scope.muteUnmuteChannelPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.muteUnmuteChannelPayload.channel_id = channelId;
          $scope.muteUnmuteChannelPayload.status = status;
          return userProfileFactory.muteUnmuteChannel($scope.muteUnmuteChannelPayload).success(function(response) {
            $rootScope.starredChannel[index].mute = status;
          });
        } else {
          ngDialog.close();
          $rootScope.clickButton('signin');
        }
      };

    }
  }
});

angular.module('flocktApp').directive("eventCard", function() {
  return {
    restrict: 'EA',
    scope: {
      details: "=",
      community: "=",
      channelid: "="
    },
    templateUrl: "../views/eventCard.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {

      $scope.init = function() {
        $scope.$storage = $localStorage;
        $scope.emotePostPayload = {};
        console.log()
      };

      $scope.getComments = function(postId, dialog) {
        $scope.currentPostId = postId;
        ngDialog.open({
          template: '../views/popup.html',
          className: 'ngdialogTheme',
          scope: $scope,
          controller: 'commentController'
        });
      };

      $scope.emotePost = function(emotion) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.emotePostPayload.community_name = $stateParams.community_name;
          $scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.emotePostPayload.post_id = $scope.details.post_id;
          $scope.emotePostPayload.emotion = emotion;
          return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
            if (response.result == "success") {
              if (emotion) {
                $scope.details.is_emoted = 1;
                $scope.details.emote_count += 1;
              } else {
                $scope.details.is_emoted = 0;
                $scope.details.emote_count -= 1;
              }
            }
          });
        } else {
          $rootScope.clickButton('signin');
        }
      };

      $scope.init();

    }
  };
});
angular.module('flocktApp').directive('linkCard', function() {
  return {
    restrict: 'EA',
    scope: {
      details: "=",
      from: "=",
      community: "=",
      channelid: "="
    },
    templateUrl: "../views/linkCard.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {

      $scope.init = function() {
        $scope.$storage = $localStorage;
        $scope.emotePostPayload = {};
      };

      $scope.getComments = function(postId, dialog) {
        $scope.currentPostId = postId;
        ngDialog.open({
          template: '../views/popup.html',
          className: 'ngdialogTheme',
          scope: $scope,
          controller: 'commentController'
        });
      };

      $scope.emotePost = function(emotion) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.emotePostPayload.community_name = $stateParams.community_name;
          $scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.emotePostPayload.post_id = $scope.details.post_id;
          $scope.emotePostPayload.emotion = emotion;
          return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
            if (response.result == "success") {
              if (emotion) {
                $scope.details.is_emoted = 1;
                $scope.details.emote_count += 1;
              } else {
                $scope.details.is_emoted = 0;
                $scope.details.emote_count -= 1;
              }
            }
          });
        } else {
          $rootScope.clickButton('signin');
        }
      };

      $scope.init();


    }
  };
});
angular.module('flocktApp').directive('textCard', function() {
  return {
    restrict: 'EA',
    scope: {
      details: "=",
      community: "=",
      channelid: "="
    },
    templateUrl: "../views/textCard.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {

      $scope.init = function() {
        $scope.$storage = $localStorage;
        $scope.emotePostPayload = {};
      };

      $scope.getComments = function(postId, dialog) {
        $scope.currentPostId = postId;
        ngDialog.open({
          template: '../views/popup.html',
          className: 'ngdialogTheme',
          scope: $scope,
          controller: 'commentController'
        });
      };

      $scope.emotePost = function(emotion) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.emotePostPayload.community_name = $stateParams.community_name;
          $scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.emotePostPayload.post_id = $scope.details.post_id;
          $scope.emotePostPayload.emotion = emotion;
          return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
            if (response.result == "success") {
              if (emotion) {
                $scope.details.is_emoted = 1;
                $scope.details.emote_count += 1;
              } else {
                $scope.details.is_emoted = 0;
                $scope.details.emote_count -= 1;
              }
            }
          });
        } else {
          $rootScope.clickButton('signin');
        }
      };

      $scope.init();

    }
  };
});

angular.module('flocktApp').directive('videoCard', function() {
  return {
    restrict: 'EA',
    scope: {},
    templateUrl: "../views/videoCard.html",
    controller: function($sce, $scope, $location, $window, $rootScope, ngDialog) {
      $scope.windowWidth = $window.innerWidth;
      $scope.config = {
        sources: [{
          src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"),
          type: "video/mp4"
        }, {
          src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"),
          type: "video/webm"
        }, {
          src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"),
          type: "video/ogg"
        }],
        tracks: [{
          src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
          kind: "subtitles",
          srclang: "en",
          label: "English",
          default: ""
        }],
        theme: "bower_components/videogular-themes-default/videogular.css",
        plugins: {
          poster: "http://www.videogular.com/assets/images/videogular.png"
        }
      };
      $scope.clickComment = function() {
        var newScope = $scope.$new();
        newScope.viewreply = function() {
          ngDialog.open({
            template: '../views/viewreply.html',
            className: 'ngdialogTheme'
          });
        };
        ngDialog.open({
          template: '../views/popup.html',
          scope: newScope,
          className: 'ngdialogTheme'
        });
      };
    }
  };
});
angular.module('flocktApp').directive('imageCard', function() {
  return {
    restrict: 'EA',
    scope: {
      details: "=",
      community: "=",
      channelid: "="
    },
    templateUrl: "../views/imageCard.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {

      $scope.init = function() {
        $scope.$storage = $localStorage;
        $scope.emotePostPayload = {};
        console.log()
      };

      $scope.getComments = function(postId, dialog) {
        $scope.currentPostId = postId;
        ngDialog.open({
          template: '../views/popup.html',
          className: 'ngdialogTheme',
          scope: $scope,
          controller: 'commentController'
        });
      };

      $scope.emotePost = function(emotion) {
        if ($scope.$storage.userDetails.hasUser) {
          $scope.emotePostPayload.community_name = $stateParams.community_name;
          $scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
          $scope.emotePostPayload.post_id = $scope.details.post_id;
          $scope.emotePostPayload.emotion = emotion;
          return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
            if (response.result == "success") {
              if (emotion) {
                $scope.details.is_emoted = 1;
                $scope.details.emote_count += 1;
              } else {
                $scope.details.is_emoted = 0;
                $scope.details.emote_count -= 1;
              }
            }
          });
        } else {
          $rootScope.clickButton('signin');
        }
      };

      $scope.init();

    }
  };
});


angular.module('flocktApp').directive("webNavbar", function() {
  return {
    restrict: "E",
    scope: {},
    templateUrl: "../views/webNavbar.html",
    controller: function($scope, $location, $window, $rootScope, ngDialog, $localStorage, $stateParams, userProfileFactory) {

      $scope.$storage = $localStorage;

      $scope.init = function() {
        if ($scope.$storage.userDetails && $scope.$storage.userDetails.hasUser) {
          $scope.userPresent = true;
        } else {
          $scope.userPresent = false;
        }
      };

      $scope.$on('userLoggedIn', function(event) {
        $scope.init();
      });

      $scope.$on('loggedOut', function(event) {
        $scope.init();
      });

      $scope.init();

    }
  }
});

angular.module('flocktApp').directive('scrollTrigger', function($window) {
    return {
        link : function(scope, element, attrs) {
            var offset = parseInt(attrs.threshold) || 0;
            var e = jQuery(element[0]);
            var doc = jQuery(document);
            angular.element(document).bind('scroll', function() {
                if (doc.scrollTop() + $window.innerHeight + offset > e.offset().top) {
                    scope.$apply(attrs.scrollTrigger);
                }
            });
        }
    };
});


angular.module('flocktApp').filter('underscoreless', function() {
  return function(input) {
    return input.replace(/_/g, ' ');
  };
});
angular.module('flocktApp').filter('removeunderscore', function() {
  return function(input) {
    input.replace(/-/g, '.');
  }
});
angular.module('flocktApp').filter('words', function() {
  return function(input, words) {
    if (isNaN(words)) {
      return input;
    }
    if (words <= 0) {
      return '';
    }
    if (input) {
      var inputWords = input.split(/\s+/);
      if (inputWords.length > words) {
        input = inputWords.slice(0, words).join(' ') + '\u2026';
      }
    }
    return input;
  };
});
