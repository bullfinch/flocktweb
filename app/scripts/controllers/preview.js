angular.module('flocktApp').controller('previewController', function($rootScope, $scope, $window, $timeout, $location, $localStorage, userProfileFactory, ngDialog) {
      $scope.windowSize = $window.innerWidth;
      $scope.windowHeight = $window.innerHeight;
            $scope.loadUsers = function() {

        // Use timeout to simulate a 650ms request.
        return $timeout(function() {

          $scope.users = $scope.users || [{
            id: 1,
            name: 'Scooby Doo'
          }, {
            id: 2,
            name: 'Shaggy Rodgers'
          }, {
            id: 3,
            name: 'Fred Jones'
          }, {
            id: 4,
            name: 'Daphne Blake'
          }, {
            id: 5,
            name: 'Velma Dinkley'
          }];

        }, 650);
      };
      $scope.channelClick = function(id) {
        ngDialog.close()
        ngDialog.open({
          template: id,
          className: 'ngdialog-theme-default sideBarMenu',
          scope: $scope,
        });
        $scope.currentTab = id;
      }
      $scope.clickToclose = function(id) {
        ngDialog.close()
        $scope.currentTab = id;
      }
});