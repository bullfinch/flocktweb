angular.module('flocktApp').controller('commntCtrl', function($sce, $scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {

  $scope.$storage = $localStorage;

  $scope.init = function() {
    $rootScope.loading = true;
    $scope.$emit('parentCalls', 'updateDetails');
    $scope.channelPostPayload = {};
    $scope.getChannelDetails();
  };

  $scope.getChannelDetails = function() {
    $scope.channelPostPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.channelPostPayload.community_name = $stateParams.community_name;
    $scope.channelPostPayload.channel_id = $stateParams.channel_id;
    return userProfileFactory.getChannelPosts($scope.channelPostPayload).success(function(response) {
      $scope.channelPosts = response.post;
      $rootScope.loading = false;
    });
  };

  var counter = 0;

  $scope.$watch(function() {
    if ($scope.$storage.userDetails && $scope.$storage.userDetails.sessionKey && counter == 0) {
      counter += 1;
      $scope.init();
    }
  });

  $scope.goToCreatePost = function() {
    $location.path('/newPost/' +  $scope.channelPostPayload.community_name);
  };

});
