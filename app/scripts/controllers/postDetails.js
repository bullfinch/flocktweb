angular.module('flocktApp').controller('postDetailsController', function($rootScope, $scope, userProfileFactory, $stateParams, $localStorage) {

  $scope.$storage = $localStorage;

	$scope.init = function(){
    $rootScope.loading = true;
		$scope.$emit('parentCalls', 'updateDetails');
		$scope.postDetailsPayload = {};
		$scope.emotePostPayload = {};
		$scope.getPostDetails();
	};

	$scope.getPostDetails = function() {
    $scope.postDetailsPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.postDetailsPayload.community_name = $stateParams.community_name;
		$scope.postDetailsPayload.channel_id = $stateParams.channel_id;
		$scope.postDetailsPayload.post_id = $stateParams.post_id;
    return userProfileFactory.getPostDetails($scope.postDetailsPayload).success(function(response) {
      $scope.postDetails = response;
      $rootScope.loading = false;
      console.log(JSON.stringify($scope.postDetails));
    });
  };

	$scope.getComments = function(postId, dialog) {
		$scope.currentPostId = postId;
		ngDialog.open({
			template: '../views/popup.html',
			className: 'ngdialogTheme',
			scope: $scope,
			controller: 'commentController'
		});
	};

	$scope.emotePost = function(emotion) {
		if($scope.$storage.userDetails.hasUser){
			$scope.emotePostPayload.community_name = $stateParams.community_name;
			$scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
			$scope.emotePostPayload.post_id = $scope.details.post_id;
			$scope.emotePostPayload.emotion = emotion;
			return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
				if (response.result == "success") {
					if(emotion){
						$scope.details.is_emoted = 1;
						$scope.details.emote_count += 1;
					}else{
						$scope.details.is_emoted = 0;
						$scope.details.emote_count -= 1;
					}
				}
			});
		}else {
				$rootScope.clickButton('signin');
		}
	};

	$scope.init();

});
