angular.module('flocktApp').controller('communityInfoController', function($rootScope, $scope, $window, $timeout, $location, $localStorage, userProfileFactory, $stateParams, $filter) {

  $rootScope.currentPage = 'communityInfo';
  $scope.windowSize = $window.innerWidth;

  $scope.init = function() {
    $rootScope.loading = true;
    $scope.$storage = $localStorage;
    $scope.$emit('parentCalls', 'updateDetails');
    $scope.communityInfoPayload = {};
    $scope.joinCommunityPayload = {};
    $scope.getMemberDetailsPayload = {};
    $scope.communityPostPayload = {};
    $scope.updateUserPayload = {};
    if($scope.windowSize <= 768){
      $scope.showHeader = false;
    }else {
      $scope.showHeader = true;
    }
    $scope.imageBaseUrl = $scope.$storage.userDetails.image_base_url;
    $scope.myDate = new Date();
    $scope.minDate = new Date(
      $scope.myDate.getFullYear(),
      $scope.myDate.getMonth() - 2,
      $scope.myDate.getDate());
    $scope.getCommunityPost();
  };

  $scope.goToChannel = function() {
    $location.path('/channel');
  };

  $scope.joinCommunity = function(status) {
    var datePresent = angular.isNumber($scope.$storage.userDetails.dob);
    if (($scope.$storage.userDetails && $scope.$storage.userDetails.hasUser && (datePresent || $scope.$storage.userDetails.dob)) || status == 'update') {
      $scope.joinCommunityPayload.key = $scope.$storage.userDetails.sessionKey;
      $scope.joinCommunityPayload.community_name = $stateParams.community_name;
      $scope.joinCommunityPayload.showGender = "1";
      $scope.joinCommunityPayload.showBirthday = "1";
      $scope.joinCommunityPayload.showAge = "0";
      if ($scope.$storage.userDetails.dob) {
        $scope.joinCommunityPayload.dob = $filter('date')($scope.$storage.userDetails.dob, 'yyyy-MM-dd');
      } else {
        $scope.joinCommunityPayload.dob = $filter('date')($scope.joinCommunityPayload.dob, 'yyyy-MM-dd');
      }
      return userProfileFactory.setProfileDetails($scope.joinCommunityPayload).success(function(response) {
        if (response.value == 'success' || response.value == 'Success') {
          $rootScope.showCustomToast("Joined the  Community");
          if (!$scope.$storage.userDetails.dob) {
            var timeStamp = $filter('date')($scope.updateUserPayload.dob, 'fullDate');
            $scope.$storage.userDetails.dob = new Date(timeStamp).getTime();
          }
          $rootScope.communityDetails.is_member = true;
          $scope.getCommunityPost();
        } else if (response.value == 'error') {
          $rootScope.showCustomToast(response.errors);
          // $scope.userDetailsError = response.error;
        }
      }).error(function(response) {});
    } else if ($scope.$storage.userDetails && $scope.$storage.userDetails.hasUser && !datePresent && !$scope.$storage.userDetails.dob) {
      $scope.ageError = "Please update your age";
      $scope.updateAgeSection = true;
    } else {
      $rootScope.clickButton('signin');
    }
  };

  $scope.getCommunityPost = function() {
    $scope.communityPostPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.communityPostPayload.community_name = $stateParams.community_name;
    return userProfileFactory.getCommuntiyPosts($scope.communityPostPayload).success(function(response) {
      $scope.communityPosts = response.post;
      $scope.isMember = response.is_member;
      $rootScope.loading = false;
      // console.log(angular.fromJson($scope.communityPosts[0].content));
    });
  };

  $scope.scrollTriggered = function(){
    if($scope.windowSize <= 768){
      if($window.pageYOffset >= 188){
        $scope.showHeader = true;
      }else {
        $scope.showHeader = false;
      }
    }
  };

  var counter = 0;

  $scope.$watch(function() {
    if ($scope.$storage.userDetails && $scope.$storage.userDetails.sessionKey && counter == 0) {
      counter += 1;
      $scope.init();
    }
  });

  $scope.$on('userLoggedIn', function(event) {
    $scope.init();
  });


  $scope.$on('loggedOut', function(event) {
    $scope.init();
  });

  $scope.goToCreatePost = function(community) {
    $location.path('/newPost/' + community);
  };
});
