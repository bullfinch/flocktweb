angular.module('flocktApp').controller('postCtrl', function($scope, $localStorage, $stateParams, userProfileFactory, ngDialog, $window, $timeout, $rootScope, Upload, $filter, $location) {
  $scope.hideEventDetails = false;

  $scope.init = function() {
    $scope.windowHeight = $window.innerHeight;
    $scope.selectChannelPopupHeight = $scope.windowHeight;
    $scope.$storage = $localStorage;
    $scope.channels = null;
    $scope.extractUrlPayload = {};
    $scope.createPostPayload = {};
    $scope.imageUploadPayload = {};
    $scope.channelPayload = {};
    $scope.eventPayload = {};
    $scope.currentCommunity = $stateParams.community_name;
    if(!$scope.$storage.userDetails.hasUser){
      $rootScope.showCustomToast("Login to create a post.");
      $location.path('/communityInfo/'+$scope.currentCommunity);
    }
    $scope.eventPayload.start_date = new Date();
    $scope.eventPayload.end_date = new Date();
    $scope.urlData = {};
    $scope.$emit('parentCalls', 'updateDetails');
    $scope.createPostPayload.images = [];
    $scope.myDate = new Date();
    $scope.createPostPayload.post_type = "Post";
    $scope.getChannels();
  };

  $scope.checkboxClick = function() {
    $scope.hideEventDetails = !$scope.hideEventDetails;
  };

  var regex1 = new RegExp(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
  var regex2 = new RegExp(/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);

  $scope.checkForUrl = function(from) {
    if (regex1.test($scope[from]) || regex2.test($scope[from])) {
      $scope.extractUrl(from);
    }
  };

  $scope.extractUrl = function(from) {
    $scope.extractUrlPayload.url = $scope[from];
    return userProfileFactory.extractUrl($scope.extractUrlPayload).success(function(response) {
      if (response.value == 'success' || response.value == "Success") {
        $scope.showPreview = true;
        $scope.createPostPayload = {
          "title": response.title,
          "base_url": response.base,
          "content": response.description,
          "post_url": $scope.extractUrlPayload.url
        };
        $scope.createPostPayload.images = [];
        $scope.createPostPayload.images.push(response.image);
        $scope.createPostPayload.post_type = "Link";
      }
    });
  };

  $scope.getChannels = function() {
    $scope.channelPayload.community_name = $stateParams.community_name;
    $scope.channelPayload.key = $scope.$storage.userDetails.sessionKey;
    return userProfileFactory.getChannelsList($scope.channelPayload).success(function(response) {
      $scope.currentChannels = response;
    });
  };

  $scope.createPost = function() {
    $scope.createPostPayload.post_type_id = "1";
    $scope.createPostPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.createPostPayload.type = "create";
    $scope.createPostPayload.community_name = $scope.currentCommunity;
    $scope.createPostPayload.selected_channel = $scope.selected_channel.channel_id;
    return userProfileFactory.createPost($scope.createPostPayload).success(function(response) {
      $rootScope.showCustomToast("Post Created");
      $location.path('/communityInfo/'+$scope.currentCommunity)
    });
  };

  $scope.loadUsers = function() {
    return $timeout(function() {
      $scope.channels = $scope.channels || $rootScope.channels;
    }, 650);
  };

  $scope.nextBtnClick = function() {
    if ($scope.eventPost) {
      $scope.createPostPayload.post_type = "Event";
      $scope.createPostPayload.start_date = $filter('date')($scope.eventPayload.start_date, 'yyyy-MM-dd');
      $scope.createPostPayload.end_date = $filter('date')($scope.eventPayload.end_date, 'yyyy-MM-dd');
      $scope.createPostPayload.start_time = $filter('date')($scope.eventPayload.start_time, 'shortTime');
      $scope.createPostPayload.duration = $scope.eventPayload.duration;
      $scope.createPostPayload.city = $scope.eventPayload.city;
      $scope.createPostPayload.location = $scope.eventPayload.location;
    } else {
      $scope.eventPayload = {};
    }
    if ($scope.createPostPayload.post_type != "Link") {
      $scope.createPostPayload.title = $scope.title;
      $scope.createPostPayload.content = $scope.description;
    }
    $scope.selectChannelSection = true;
  };

  $scope.uploadFiles = function(files, errFiles) {
    $scope.files = files;
    $scope.errFiles = errFiles;
    angular.forEach(files, function(file) {
      file.upload = Upload.upload({
        url: $rootScope.baseUrl + '/media/' + $scope.currentCommunity,
        data: {
          "media": file,
          "media_type": "Post",
          'position': "general'"
        }
      });
      file.upload.then(function(response) {
        $timeout(function() {
          $scope.createPostPayload.images.push(response.data.url);
          file.result = response.data;
        });
      }, function(response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
      }, function(evt) {
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });
    });
  }


  $scope.previewPost = function() {
    if ($scope.selected_channel) {
      $scope.selectChannelError = false;
      $scope.createPostPayload.selected_channel = $scope.selected_channel.channel_name;
      $scope.preview = true;
    } else {
      $scope.selectChannelError = true;
    }
  };

  var counter = 0;

  $scope.$watch(function() {
    if ($scope.$storage.userDetails && $scope.$storage.userDetails.sessionKey && counter == 0) {
      counter += 1;
      $scope.init();
    }
  });

  $scope.$on('loggedOut', function(event) {
    $rootScope.showCustomToast("Login to create a post.");
    $location.path('/communityInfo/'+$scope.currentCommunity);
  });

});
