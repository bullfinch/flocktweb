angular.module('flocktApp').controller('communityController', function($rootScope, $scope, $window, $timeout, $location, $localStorage, userProfileFactory,$stateParams) {
  $scope.windowWidth = $window.innerWidth;
  if ($scope.windowWidth <= 767) {
    $scope.windowHeight = $window.innerHeight;
  } else {
    $scope.windowHeight = 'auto';
  }
  $scope.$storage = $localStorage;

  $scope.init = function() {
    $rootScope.loading = true;
    $scope.communityPayload = {};
    $scope.communityPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.getCommunityList();
    console.log($scope.$storage.userDetails);
    $scope.imageBaseUrl = $scope.$storage.userDetails.image_base_url;
    $rootScope.currentPage = 'communityPage';
    $scope.discoverCommunityLimit = 1;
    $scope.discoverCommunityBegin = 0;
  };

  $scope.discoverNextCard = function(swipe) {
    if (swipe == 'leftSwipe') {
      if ($scope.discoverCommunityBegin < $scope.otherCommunities.length - 1) {
        $scope.discoverCommunityBegin += 1;
      } else {
        $scope.discoverCommunityBegin = 0;
      }
    } else if (swipe == 'rightSwipe') {
      if ($scope.discoverCommunityBegin == 0) {
        $scope.discoverCommunityBegin = $scope.otherCommunities.length - 1;
      } else {
        $scope.discoverCommunityBegin -= 1;
      }
    }
  };

  $scope.getCommunityList = function() {
    return userProfileFactory.getCommunitiesList($scope.communityPayload).success(function(response) {
      $scope.myCommunities = response.my;
      $scope.otherCommunities = response.other;
      $rootScope.loading = false;
    });
  };

  $scope.convertToTimeStamp = function(time){
    var timeStamp = new Date(time).getTime();
    return timeStamp
  };

  var counter = 0;

  $scope.$watch(function() {
    if ($scope.$storage.userDetails && $scope.$storage.userDetails.sessionKey && counter == 0) {
      counter += 1;
      $scope.init();
    }
  });

  $scope.$on('userLoggedIn', function(event) {
    $scope.init();
  });

  $scope.goToCommunityInfo = function(community) {
    var url = 'communityInfo/' + community;
    $window.open(url, '_blank');
  };

});
