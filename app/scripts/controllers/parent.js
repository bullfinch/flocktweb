angular.module('flocktApp').controller('parentController', function($scope, $rootScope, userProfileFactory, deviceDetector, $window, $localStorage, $mdSidenav, $stateParams, $location, ngDialog, $mdToast) {

  $rootScope.openSideNavPanel = function() {
    $scope.showMenu = true;
    $mdSidenav('left').open();
  };

  $rootScope.closeSideNavPanel = function() {
    $mdSidenav('left').close();
    $scope.showMenu = false;
  };

  $scope.$storage = $localStorage;

  $scope.init = function() {
    $rootScope.loading = true;
    $rootScope.pageLoaded = false;
    $scope.communityDetailsPayload = {};
    $rootScope.reloading = true;
    $scope.validateKeyPayload = {};
    $rootScope.baseUrl = "http://flockt.net/flocktapi";
    console.log($scope.$storage.userDetails);
    $scope.sessionPayload = {};

    if ($scope.$storage.userDetails && $scope.$storage.userDetails.sessionKey) {} else {
      window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection; //compatibility for firefox and chrome
      var pc = new RTCPeerConnection({
          iceServers: []
        }),
        noop = function() {};
      pc.createDataChannel(""); //create a bogus data channel
      pc.createOffer(pc.setLocalDescription.bind(pc), noop); // create offer and set local description
      pc.onicecandidate = function(ice) { //listen for candidate events
        if (!ice || !ice.candidate || !ice.candidate.candidate) return;
        var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
        $scope.sessionPayload.device_ip = myIP;
        $scope.createSession();
        pc.onicecandidate = noop;
      };
    }
  };

  $scope.createSession = function() {
    $scope.sessionPayload.type = "session";
    $scope.sessionPayload.device_type = deviceDetector.device + ", " + deviceDetector.os + ", " + deviceDetector.browser + " " + deviceDetector.browser_version;
    return userProfileFactory.createSession($scope.sessionPayload).success(function(response) {
      $scope.$storage.userDetails = {
        "sessionKey": response.key,
        "device_type": $scope.sessionPayload.device_type,
        "device_ip": $scope.sessionPayload.device_ip,
        "image_base_url": response.image_base
      };
      console.log($scope.$storage.userDetails);
      $scope.$emit('broadcastCalls', "userLoggedIn");
    }).error(function(response) {});
  };

  $scope.close = function() {
    $mdSidenav('left').close();
  };

  $rootScope.$on('broadcastCalls', function(event, message) {
    $scope.$broadcast(message);
  });

  $rootScope.$on('parentCalls', function(event, message) {
    if (message == 'updateDetails' && $stateParams.key && $scope.$storage.userDetails) {
      $rootScope.disableWebMenu = true;
      if ($stateParams.key == $scope.$storage.userDetails.sessionKey) {
        $scope.getCommunityDetails();
      } else {
        $scope.validateKey();
      }
    } else if (message == 'updateDetails' && !$stateParams.key) {
      $scope.getCommunityDetails();
    }
  });

  $scope.validateKey = function() {
    $scope.validateKeyPayload.key = $scope.$storage.userDetails.sessionKey;
    return userProfileFactory.validateUserKey($scope.validateKeyPayload).success(function(response) {
      $scope.$storage.userDetails.sessionKey = $stateParams.key;
      $scope.$storage.userDetails.hasUser = true;
      // $scope.$storage.userDetails.name = response.name;
      $scope.$storage.userDetails.dob = new Date(response.dob).getTime();
      $scope.$emit('broadcastCalls', "userLoggedIn");
    });
  };

  $scope.getCommunityDetails = function() {
    $scope.communityDetailsPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.communityDetailsPayload.value = "all";
    $scope.communityDetailsPayload.community_name = $stateParams.community_name;
    return userProfileFactory.getCommuntiyDetails($scope.communityDetailsPayload).success(function(response) {
      $rootScope.communityName = response.display_name;
      $rootScope.communityDetails = response;
      $rootScope.pageLoaded = true;
      $scope.$emit('broadcastCalls', "updateChannelList");
    });
  };

  $rootScope.goToPostDetails = function() {
    $location.path('/postDetails');
  };

  $rootScope.showCustomToast = function(message) {
    $rootScope.toastMessage = message;
    $mdToast.show({
      hideDelay: 5000,
      position: 'top right',
      templateUrl: '../views/toast.html'
    });
  };

  $rootScope.logout = function() {
    $scope.loggeOutPayload = {
      "key": $scope.$storage.userDetails.sessionKey
    }
    return userProfileFactory.logout($scope.loggeOutPayload).success(function(response) {
      $scope.$storage.userDetails = {};
      $window.location.reload();
    });
  };

  $scope.init();

});
