angular.module('flocktApp').controller('commentController', function($sce, $scope, $location, $window, $rootScope, ngDialog, $stateParams, $localStorage, userProfileFactory) {
  $scope.init = function() {
    $scope.windowSize = $window.innerWidth;
    if($scope.windowSize <768){
      $scope.windowHeight = $window.innerHeight-100;
      $scope.commentHeight = $window.innerHeight - 185;
      $scope.topMargin = 0;
    }else {
        $scope.commentHeight = 400;
        $scope.windowHeight = $window.innerHeight - ($window.innerHeight*.05);
        $scope.topMargin = $window.innerHeight*.05;
    }
    $scope.$storage = $localStorage;
    $scope.commentsPayload = {};
    $scope.addCommentPayload = {};
    $scope.emoteCommentPayload = {};
    $scope.getCommentReplyPayload = {};
    $scope.replyToComment = {};
    $scope.emotePostPayload = {};
    $scope.viewreply = false;
    $scope.getCommentsCall($scope.currentPostId);
  };

  $scope.getCommentsCall = function(postId) {
    $scope.viewreply = false;
    $scope.commentsPayload.community_name = $stateParams.community_name;
    $scope.commentsPayload.post_id = postId;
    $scope.commentsPayload.key = $scope.$storage.userDetails.sessionKey;
    return userProfileFactory.getComments($scope.commentsPayload).success(function(response) {
      if (response.value == "success" || response.value == "Success") {
        $scope.comments = response.comments;
      }
    });
  };

  $scope.addComment = function() {
    $scope.addCommentPayload.community_name = $stateParams.community_name;
    $scope.addCommentPayload.post_id = $scope.commentsPayload.post_id;
    $scope.addCommentPayload.key = $scope.$storage.userDetails.sessionKey;
    return userProfileFactory.addComments($scope.addCommentPayload).success(function(response) {
      if (response.result == "created") {
        $scope.getCommentsCall($scope.addCommentPayload.post_id, false);
        $scope.details.comment_count += 1;
        $scope.addCommentPayload.content = "";
      }
    });
  };

  $scope.emoteComment = function(status, commentId, index) {
    if($scope.$storage.userDetails.hasUser){
      $scope.emoteCommentPayload.community_name = $stateParams.community_name;
      $scope.emoteCommentPayload.post_id = $scope.commentsPayload.post_id;
      $scope.emoteCommentPayload.key = $scope.$storage.userDetails.sessionKey;
      $scope.emoteCommentPayload.isComment = 1;
      $scope.emoteCommentPayload.emotion = status;
      $scope.emoteCommentPayload.comment_id = commentId;
      return userProfileFactory.emoteComment($scope.emoteCommentPayload).success(function(response) {
        if (response.result == "success") {
          if (status == 1) {
            $scope.comments[index].emote_count += 1;
          } else {
            $scope.comments[index].emote_count -= 1;
          }
        }
      });
    }else {
      $rootScope.clickButton('signin');
    }
  };

  $scope.getCommentReply = function(commentId, index) {
    $scope.curretComment = $scope.comments[index];
    $scope.replies = [];
    $scope.indexValue = index;
    $scope.viewreply = true;
    $scope.getCommentReplyPayload.community_name = $stateParams.community_name;
    $scope.getCommentReplyPayload.key = $scope.$storage.userDetails.sessionKey;
    $scope.getCommentReplyPayload.comment_id = commentId;
    return userProfileFactory.getCommentReply($scope.getCommentReplyPayload).success(function(response) {
      if (response.value == "success") {
        $scope.replies = response.replies;
      }
    });
  };

  $scope.hideReply = function(){
    $scope.viewreply = false;
  };

  $scope.replyForComment = function(index) {
    $scope.replyToComment.community_name = $stateParams.community_name;
    $scope.replyToComment.key = $scope.$storage.userDetails.sessionKey;
    $scope.replyToComment.parent = $scope.curretComment.comment_id ;
    $scope.replyToComment.post_id = $scope.curretComment.post_id;
    return userProfileFactory.replyForComment($scope.replyToComment).success(function(response) {
      if (response.result == "created") {
        $scope.getCommentReply($scope.getCommentReplyPayload.comment_id, $scope.indexValue);
        $scope.replyToComment.content = "";
      }
    });
  };

  $scope.emotePost = function(emotion) {
    if($scope.$storage.userDetails.hasUser){
      $scope.emotePostPayload.community_name = $stateParams.community_name;
      $scope.emotePostPayload.key = $scope.$storage.userDetails.sessionKey;
      $scope.emotePostPayload.post_id = $scope.details.post_id;
      $scope.emotePostPayload.emotion = emotion;
      return userProfileFactory.emotePost($scope.emotePostPayload).success(function(response) {
        if (response.result == "success") {
          if(emotion){
            $scope.details.is_emoted = 1;
            $scope.details.emote_count += 1;
          }else{
            $scope.details.is_emoted = 0;
            $scope.details.emote_count -= 1;
          }
        }
      });
    }else {
        $rootScope.clickButton('signin');
    }
  };

  $scope.init();
});
