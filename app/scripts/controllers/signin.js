angular.module('flocktApp').controller('signInController', function($rootScope, $scope, $window, $timeout, $location, $localStorage, userProfileFactory, ngDialog) {

  $scope.$storage = $localStorage;
  var stopped;

  $scope.init = function() {
    $scope.windowHeight = $window.innerHeight;
    $scope.loginPayload = {};
    $scope.enterNumberSectionSignIn = true;
    $scope.counter = 0;
    $scope.inputType = "password";
    $scope.signUpPayload = {};
    $scope.verifyOtpPayload = {};
    $scope.createPinPayload = {};
    $scope.updateUserPayload = {};
    $scope.mobile = {};
    $scope.signupMobile = {};
    $scope.mobile.country = "91";
  };

  $scope.verifyPin = function() {
    if ($scope.loginPayload.pin) {
      $scope.loginPayload.type = "login";
      $scope.loginPayload.key = $scope.$storage.userDetails.sessionKey;
      $scope.loginPayload.mobile = $scope.mobile.country + $scope.mobile.mobile;
      return userProfileFactory.createSession($scope.loginPayload).success(function(response) {
        if (response.value == "success" || response.value == "Success") {
          $scope.pinError = " ";
          $rootScope.showCustomToast("Sign in Successful");
          $scope.$storage.userDetails.hasUser = true;
          $scope.$storage.userDetails.name = response.name;
          $scope.$storage.userDetails.dob = new Date(response.dob).getTime();
          console.log($scope.$storage.userDetails);
          // $scope.$storage.userDetails.user_id = response.user_id;
          $scope.$emit('broadcastCalls', "userLoggedIn");
          ngDialog.closeAll();
        } else if (response.value == 'error') {
          $scope.pinError = response.error;
        }
      }).error(function(response) {});
    } else {
      $scope.pinError = "Enter a Pin";
    }
  };

  $scope.goToSignup = function() {
    $scope.signupMobile.signupMobile = $scope.mobile.mobile;
    $scope.signupMobile.signupCountryCode = $scope.mobile.country;
    $scope.signUpPayload.mobile = $scope.signupMobile.signupCountryCode + $scope.signupMobile.signupMobile;
    $scope.signupPage1 = true;
    $scope.enterPinSignIn = false;
    $scope.$emit('broadcastCalls', "userLoggedIn");
  };

  $scope.numberSubmit = function() {
    if ($scope.mobile.mobile) {
      $scope.numberError = "";
      $scope.enterPinSignIn = true;
      $scope.enterNumberSectionSignIn = false;
    }else{
      $scope.numberError = "Enter Number";
    }
  }

  $scope.loadUsers = function() {
    return $timeout(function() {
      $scope.countryList = ["93", "355", "213", "1-684", "376", "244", "1-264", "1-268", "54", "374", "297", "43", "994", "1-242", "973", "880", "1-246", "375", "32", "501", "229", "1-441", "975", "591", "387", "267", "55", "246", "1-284", "673", "359", "226", "257", "855", "237", "1", "238", "1-345", "236", "235", "56", "86", "61", "57", "269", "242", "243", "682", "506", "385", "53", "599", "357", "420", "225", "45", "253", "1-767", "1-809,1-829,1-849", "593", "20", "503", "240", "291", "372", "251", "298", "679", "33", "594", "689", "262", "241", "220", "995", "49", "233", "350", "30", "299", "1-473", "1-671", "502", "44", "224", "245", "592", "509", "504", "852", "36", "354", "91", "62", "98", "964", "353", "972", "39", "1-876", "81", "962", "254", "686", "965", "996", "856", "371", "961", "266", "231", "218", "423", "370", "352", "853", "389", "261", "265", "60", "960", "223", "356", "692", "596", "222", "230", "52", "691", "373", "377", "976", "382", "1-664", "212", "258", "95", "264", "674", "977", "31", "687", "64", "505", "227", "234", "683", "672", "850", "1-670", "968", "92", "680", "970", "507", "675", "595", "51", "63", "870", "48", "351", "974", "40", "7", "250", "685", "378", "966", "221", "381", "248", "232", "65", "1-721", "421", "386", "677", "252", "27", "500", "82", "211", "34", "94", "290 ", "1-869", "1-758", "590", "508", "1-784", "249", "597", "47", "268", "46", "41", "963", "239", "886", "992", "255", "66", "670", "228", "690", "676", "1-868", "216", "90", "993", "1-649", "688", "1-340", "256", "380", "971", "598", "998", "678", "39-06", "58", "84", "681", "967", "260", "263", "358"];
      $scope.mobile.country = "91";
    }, 850);
  };

  $scope.setCountryCode = function(value) {
    $scope.country = value;
  };

  $scope.countdown = function() {
    stopped = $timeout(function() {
      $scope.counter++;
      if ($scope.counter < 60) {
        $scope.countdown();
      }
    }, 1000);
  };

  $scope.toggleType = function() {
    if ($scope.inputType == 'password')
      $scope.inputType = 'text';
    else
      $scope.inputType = 'password';
  };

  $scope.stopCounter = function() {
    $timeout.cancel(stopped);
    stopped = 0;
  };

  $scope.sendOtp = function() {
    if ($scope.signUpPayload.mobile) {
      $scope.signUpPayload.type = "send-otp";
      $scope.signUpPayload.send = "sms / mail";
      $scope.signUpPayload.key = $scope.$storage.userDetails.sessionKey;
      $scope.counter = 0;
      $scope.signupPage1 = false;
      $scope.signupPage2 = true;
      $scope.countdown();
      return userProfileFactory.createSession($scope.signUpPayload).success(function(response) {

      }).error(function(response) {

      });
    }
  };

  $scope.verifyOtp = function() {
    $scope.verifyOtpPayload.type = "otp";
    $scope.verifyOtpPayload.mobile = $scope.signUpPayload.mobile;
    $scope.verifyOtpPayload.key = $scope.$storage.userDetails.sessionKey;
    return userProfileFactory.createSession($scope.verifyOtpPayload).success(function(response) {
      if (response.value == 'success' || response.value == 'Success') {
        $scope.signupPage2 = false;
        $scope.signupPage3 = true;
        $scope.errorMessageVerifyOtp = " ";
      } else if (response.value == 'error') {
        $scope.errorMessageVerifyOtp = response.error;
      }
    }).error(function(response) {

    });
  };

  $scope.updatePIN = function() {
    $scope.errorMessage = "";
    if ($scope.createPinPayload.pin == $scope.createPinPayload.re_pin) {
      $scope.createPinPayload.type = "pin";
      $scope.createPinPayload.mobile = $scope.verifyOtpPayload.mobile;
      $scope.createPinPayload.key = $scope.$storage.userDetails.sessionKey;
      return userProfileFactory.createSession($scope.createPinPayload).success(function(response) {
        if (response.value == 'Success' || response.value == 'success') {
          if (!response.hasEmail) {
            $scope.signupPage3 = false;
            $scope.signupPage4 = true;
            $scope.$storage.userDetails.hasUser = true;
            $scope.$storage.userDetails.hasEmail = response.hasEmail;
          } else {
            $scope.$storage.userDetails.hasUser = true;
            $scope.$emit('broadcastCalls', "userLoggedIn");
            ngDialog.close();
          }
        } else if (response.value == 'error') {
          $scope.errorMessage = response.error;
        }
      }).error(function(response) {});
    } else {
      $scope.errorMessage = "PIN do not match with Confirm PIN";
    }
  };

  $scope.updateUserDetails = function() {
    $scope.updateUserPayload.key = $scope.$storage.userDetails.sessionKey;
    // $scope.updateUserPayload.gender = "1";
    // $scope.updateUserPayload.dob = "1991-18-07";
    // $scope.updateUserPayload.showAge = "0";
    return userProfileFactory.updateUserDetails($scope.updateUserPayload).success(function(response) {
      if (response.value == 'success' || response.value == 'Success') {
        $scope.$storage.userDetails.email = $scope.updateUserPayload.email;
        $scope.$storage.userDetails.name = $scope.updateUserPayload.name;
        $scope.$storage.userDetails.hasEmail = true;
        $scope.$emit('broadcastCalls', "userLoggedIn");
        ngDialog.close();
      }else if (response.value == 'error'){
        $scope.userDetailsError = response.error;
      }
    }).error(function(response) {});
  };

  $scope.init();

});
