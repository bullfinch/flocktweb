  angular.module('flocktApp').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('communityInfo', {
        url: '/communityInfo/:community_name?key',
        templateUrl: 'views/communityInfo.html',
        controller: 'communityInfoController'
      })
      .state('channel', {
        url: '/channel/:community_name/:channel_name/:channel_id?key',
        templateUrl: 'views/channel.html',
        controller: 'commntCtrl'
      })
      .state('newPost', {
        url: '/newPost/:community_name?key',
        templateUrl: 'views/newPost.html',
        controller:'postCtrl'
      })
      .state('community', {
        url: '/?key',
        templateUrl: 'views/community.html',
        controller: 'communityController'
      })
      .state('postDetails', {
        url: '/postDetails/:community_name/:channel_id/:post_id?key',
        templateUrl: 'views/postDetails.html',
        controller: 'postDetailsController'
      })
      .state('previewpost', {
        url: '/previewpost',
        templateUrl: 'views/previewpost.html',
        controller: 'previewController'
      })

  });
